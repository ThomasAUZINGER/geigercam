% Copyright 2014- Thomas Auzinger <thomas@auzinger.name>

function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 01-Aug-2012 14:06:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)

% Choose default command line output for GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.fig_GUI);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close fig_GUI.
function fig_GUI_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to fig_GUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uitoggletool_measure_OffCallback(handles.uitoggletool_measure, eventdata, handles);
drawnow;

if isfield(handles, 'cameraControl')
    cam = handles.cameraControl;
    cam.delete();
end

% Hint: delete(hObject) closes the figure
delete(hObject);


% --------------------------------------------------------------------
function menu_help_Callback(~, ~, ~)
% hObject    handle to menu_help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuit_about_Callback(~, ~, ~)
% hObject    handle to menuit_about (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

license = {['This program is free software: you can redistribute it and/or modify ' ...
    'it under the terms of the GNU General Public License as published by ' ...
    'the Free Software Foundation, either version 3 of the License, or ' ...
    '(at your option) any later version.'], '', ...
    ['This program is distributed in the hope that it will be useful, '  ...
    'but WITHOUT ANY WARRANTY; without even the implied warranty of ' ...
    'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the ' ...
    'GNU General Public License for more details.'], '', ...
    ['You should have received a copy of the GNU General Public License ' ...
    'along with this program. If not, see <http://www.gnu.org/licenses/>.']};

msgbox([{'Copyright Thomas Auzinger 2014 -' 'thomas@auzinger.name' ''} license], ...
    'About');


% --------------------------------------------------------------------
function uipushtool_init_ClickedCallback(hObject, ~, handles)
% hObject    handle to uipushtool_init (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'init') && ...
        isfield(handles.init, 'running') && ...
        handles.init.running == true
    return
end

if isfield(handles, 'measure') && ...
        isfield(handles.measure, 'running') && ...
        handles.measure.running == true
    return
end

handles.init.running = true;
set( handles.uitoggletool_measure, 'Enable', 'off');
guidata(hObject, handles);

if isfield(handles, 'cameraControl')
    cam = handles.cameraControl;
    cam.delete();
end

cam = CameraControl(Config.FPS);
handles.cameraControl = cam;

set(handles.text_camera_name, 'String', cam.Name);
set(handles.text_camera_fps,  'String', [num2str(cam.fps) ' fps']);
set(handles.text_camera_status, 'String', 'ACTIVE');
set(handles.text_camera_status, 'ForeGroundColor', [0 1 0]);
drawnow;

gpu = GPGPU(Config.ProbabilityConvolutionFilter, Config.NoiseThreshold, Config.SmoothingAlpha);
handles.gpuControl = gpu;

set(handles.text_gpu_name, 'String', gpu.Device.Name);
set(handles.text_gpu_status, 'String', 'ACTIVE');
set(handles.text_gpu_status, 'ForeGroundColor', [0 1 0]);
drawnow;

% Generate an initial smoothing frame and a dead pixel mask
initFrameCount = Config.InitializationFrameCount;
cam.Start();
for i = 1:initFrameCount
    frame = cam.GetFramesFromBuffer(1);
    if i == 1
        initSum = double(frame);
        gpu.Initialize(initSum, false(size(initSum, 1), size(initSum, 2)));
        mask_dead = gpu.GetHitMask(frame);
    else
        initSum = initSum + double(frame);
        mask = gpu.GetHitMask(frame);
        mask_dead = mask_dead | mask;
    end
end
cam.Stop();
initSum = max(initSum, 0);
gpu.Initialize(initSum / initFrameCount, mask_dead);

set( handles.uitoggletool_measure, 'Enable', 'on');
drawnow;

handles.init.running = false;
guidata(hObject, handles);


% --------------------------------------------------------------------
function uitoggletool_measure_OffCallback(hObject, ~, handles)
% hObject    handle to uitoggletool_measure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'measure') && ...
        isfield(handles.measure, 'running') && ...
        handles.measure.running == true
else
    return
end

if isfield(handles, 'measure') && ...
        isfield(handles.measure, 'running') && ...
        handles.measure.cancel == true
    return
end

handles.measure.cancel = true;
guidata(hObject, handles);


% --------------------------------------------------------------------
function uitoggletool_measure_OnCallback(hObject, ~, handles)
% hObject    handle to uitoggletool_measure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles, 'measure') && ...
        isfield(handles.measure, 'running') && ...
        handles.measure.running == true
    return
end

handles.measure.running = true;
handles.measure.cancel = false;
guidata(hObject, handles);

cam = handles.cameraControl;
gpu = handles.gpuControl;

hits = [];
delete( findobj('Parent', handles.uipanel_hits, '-and', 'Type', 'axes') );

l = 1;
lastHitCounts = zeros(1, Config.AverageTimeHitCounts * Config.FPS);

cam.Start();
while true
    
    drawnow;
    handles = guidata(hObject);
    if handles.measure.cancel == true
        disp('Cancel accepted');
        break
    end
    
    [frame, time, info] = cam.GetFramesFromBuffer(1);
    
    set(handles.text_measurement_time, 'String', [num2str(time) ' s ']);
    set(handles.text_measurement_frameCount, 'String', [num2str(info.FrameNumber) ' frames ']);
    set(handles.text_measurement_fps, 'String', [num2str(info.FrameNumber / time) ' fps ']);
    
    mask = gpu.GetHitMask(frame);
    [hits_temp, pixelIdxList] = Hit.ExtractHits(mask, frame);
    gpu.SmoothFrames(pixelIdxList);
    hits = cat(2, hits, hits_temp);
    
    set(handles.text_measurement_hitCount, 'String', [num2str(length(hits)) ' hits ']);
    
    lastHitCounts(l) = length(hits_temp);
    %set(handles.text_measurement_hitCountPerSecond, 'String', [num2str(length(hits)/time) ' hits/s ']);
    set(handles.text_measurement_hitCountPerSecond, 'String', ...
                            [num2str(sum(lastHitCounts(:)) / Config.AverageTimeHitCounts) ' hits/s ']);
    l = mod(l, Config.AverageTimeHitCounts * Config.FPS) + 1;
    
    if sum(lastHitCounts(:)) / Config.AverageTimeHitCounts >= Config.DangerThreshold
        uicontrol_SetLight(handles.uipanel_result, LightState.Danger, handles)
    else
        uicontrol_SetLight(handles.uipanel_result, LightState.Safe, handles)
    end

    if ~isempty(hits) && ~isempty(hits_temp) && cam.VideoInput.FramesAvailable == 0
        axes('parent', handles.uipanel_hits, 'Visible', 'off', 'Position', [0 0 1 1]);
        
        gapRatio = 0.05;
        c = Config.HitImagesCountPerSide;
        width = (1 - (c+1)*gapRatio) / c;
        
        n = 1;
        for j = max(1,length(hits)+1-c*c):length(hits)
            
            left = gapRatio + (gapRatio + width)*(mod(n-1,c));
            bottom = 1 - (gapRatio + width + (gapRatio + width)*floor((n-1)/c));
            position = [left bottom width width];
            
            h = subplot(c,c,n, 'Position', position);
            subimage(hits(j).Image);
            set(h,'XTick',[],'YTick',[]);
            set(get(h, 'XLabel'), 'String', [num2str(j) '|' num2str(hits(j).Energy)]);
            n = n+1;
        end
    end
end
cam.Stop();

uicontrol_SetLight(handles.uipanel_result, LightState.Off, handles)
handles.measure.running = false;
handles.measure.cancel = false;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function uipanel_result_CreateFcn(hObject, ~, handles)
% hObject    handle to uipanel_result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

uicontrol_SetLight(hObject, LightState.Off, handles)


% --- Sets the state of the traffic light visualization
function [] = uicontrol_SetLight(hControl, state, handles)
    
    delete( findobj('Parent', hControl, '-and', 'Type', 'axes') );
    axes('parent', hControl, 'Visible', 'off', 'Position', [0 0 1 1], 'DataAspectRatio', [1 1 1], 'XLim', [0 1], 'YLim', [0 1]);
    
    switch state
        case LightState.Off
            colors = [0.5 0.5 0.5; 1 1 0; 0.5 0.5 0.5];
            visibility = {'off' 'on' 'off'};
        case LightState.Safe
            colors = [0.5 0.5 0.5; 0.5 0.5 0.5; 0 1 0];
            visibility = {'off' 'off' 'on'};
        case LightState.Danger
            colors = [1 0 0; 0.5 0.5 0.5; 0.5 0.5 0.5];
            visibility = {'on' 'off' 'off'};
    end
    
    if ~isempty(handles)
        set(handles.text_result_danger, 'Visible', visibility{1});
        set(handles.text_result_off,    'Visible', visibility{2});
        set(handles.text_result_safe,   'Visible', visibility{3});
    end
    
    rectangle('Position', [0.03 0.69 0.28 0.28], 'Curvature', [1 1], 'FaceColor', colors(1,:), 'LineWidth', 2);
    rectangle('Position', [0.03 0.36 0.28 0.28], 'Curvature', [1 1], 'FaceColor', colors(2,:), 'LineWidth', 2);
    rectangle('Position', [0.03 0.03 0.28 0.28], 'Curvature', [1 1], 'FaceColor', colors(3,:), 'LineWidth', 2);