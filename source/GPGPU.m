classdef GPGPU < handle
    %GPGPU Provides GPU routines
    %   Provides GPU filtering, temporal smoothing, and hit probabilities
    
    % Copyright 2014- Thomas Auzinger <thomas@auzinger.name>
    
    %% Constructor
    methods
        function obj = GPGPU(filter, noise_threshold, smoothing_alpha)
            disp('Initializing GPU memory...');
            
            narginchk(3, 3);
            %TODO asserts
            
            obj.Device = gpuDevice();
            disp(['Using a ' obj.Device.Name '.']);
            reset(obj.Device);
            
            filter = filter / sum(filter(:)); % Normalize filter
            obj.filter = gpuArray(single(filter));
            obj.NOISE_THRESHOLD = noise_threshold;
            obj.ALPHA = smoothing_alpha;
            
            disp('Initializing GPU memory finished.');
        end
    end
    
    %% Destructor
    methods
        function delete(obj)
            reset(obj.Device);
        end
    end
    
    %% Private properties
    properties (SetAccess = private, GetAccess = private)
        NOISE_THRESHOLD
        ALPHA
        
        filter
        frame_current
        frame_smoothed
        mask_dead
    end
    
    %% Readonly public properties
    properties (SetAccess = private)
        Device
    end
    
    %% Public methods
    methods
        function [] = Initialize(obj, smoothing, deadPixelMask)
            %TODO assertion on frame size
            obj.frame_smoothed = gpuArray(smoothing);
            obj.frame_smoothed = single(obj.frame_smoothed);
            
            obj.mask_dead = gpuArray(~repmat(deadPixelMask, [1 1 3]));
            obj.mask_dead = single(obj.mask_dead);
        end
   
        function hit_mask = GetHitMask(obj, frame)
            %TODO assertion on frame size
            
            obj.frame_current = gpuArray(frame);
            obj.frame_current = single(obj.frame_current);
            
            temp_frame_current = obj.frame_current;
            temp_frame_current = temp_frame_current .* obj.mask_dead;
            probabilities_frame = (temp_frame_current - (obj.frame_smoothed + 0.5)) ./ (sqrt(obj.frame_smoothed + 0.5)); %TODO Add explanation
            
            probabilities_filtered = gpuArray.zeros( size(frame,1), size(frame,2), size(frame,3),'single');
            probabilities_filtered(:,:,1) = 0.1 * conv2(probabilities_frame(:,:,1), obj.filter, 'same');
            probabilities_filtered(:,:,2) = 0.1 * conv2(probabilities_frame(:,:,2), obj.filter, 'same');
            probabilities_filtered(:,:,3) = 0.1 * conv2(probabilities_frame(:,:,3), obj.filter, 'same');
            
            probabilities_combined = 0.001 * probabilities_frame(:,:,1) .* ...
                                             probabilities_frame(:,:,2) .* ...
                                             probabilities_frame(:,:,3) .* ...
                                             probabilities_filtered(:,:,1) .* ...
                                             probabilities_filtered(:,:,2) .* ...
                                             probabilities_filtered(:,:,3);
                                             
            mask_hits = probabilities_combined > obj.NOISE_THRESHOLD;
            
            hit_mask = gather(mask_hits);
        end
        
        function [] = SmoothFrames(obj, hitPixelList)
            indices = gpuArray(hitPixelList);
            
            num_pixel = size(obj.frame_current, 1) * size(obj.frame_current, 2);
            offset_R = 0 * num_pixel;
            offset_G = 1 * num_pixel;
            offset_B = 2 * num_pixel;
            
            obj.frame_current(indices + offset_R) = obj.frame_smoothed(indices);
            obj.frame_current(indices + offset_G) = obj.frame_smoothed(indices);
            obj.frame_current(indices + offset_B) = obj.frame_smoothed(indices);
            
            obj.frame_smoothed = obj.frame_smoothed + (obj.frame_current - obj.frame_smoothed) * obj.ALPHA; %TODO Explanation
        end
    end
    
end

