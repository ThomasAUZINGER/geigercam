classdef CameraControl < handle
    %CAMERACONTROL Geigercam control
    %   Covers initialization and video frame aquisition of the GeigerCam
    
    % Copyright 2014- Thomas Auzinger <thomas@auzinger.name>
    
    %% Constructor
    methods
        function obj = CameraControl(fps)
            disp('Initializing camera...');
            
            narginchk(1, 1);
            assert(fps > 0, 'CameraControl:Constructor:ArgChk', 'Invalid input argument.');
            
            info = imaqhwinfo('winvideo',1);
            %vid = videoinput('winvideo', 1, 'RGB24_1280x720');
            vid = videoinput('winvideo', 1, 'YUY2_1280x720');
            src = getselectedsource(vid);
            
            src.FrameRate = num2str(fps);
            src.BacklightCompensation = 'off';
            src.Brightness = 128;
            src.Contrast = 32;
            src.ExposureMode = 'manual';
            src.Exposure = -9;
            src.Gain = 100;
            src.Saturation = 32;
            src.Sharpness = 0;
            src.WhiteBalanceMode = 'manual';
            src.WhiteBalance = 5800;

            vid.ReturnedColorspace = 'rgb';
            vid.TriggerRepeat = Inf;
            vid.FramesPerTrigger = 1;
            vid.FrameGrabInterval = 1;
            vid.Timeout = 100;
            
            obj.Name = info.DeviceName;
            obj.VideoInput = vid;
            obj.VideoSource = src;
            obj.fps = fps;
            obj.MaxFrameCount = vid.Timeout * fps;
            
            disp('Initializing camera finished.');
        end
    end
    
    %% Destuctor
    methods
        function delete(obj)
            disp('Remove camera...');
            delete(obj.VideoInput);
            disp('Remove camera finished.');
        end
    end
    
    %% Readonly public properties
    properties (SetAccess = private)
        Name
        VideoInput
        VideoSource
        fps
        MaxFrameCount
    end
    
    %% Readonly public dependent properties
    properties (SetAccess = private, Dependent = true)
        X
        Y
        C
        FrameSize
        OptionString
    end
    
    methods
        function resolution_X = get.X(obj)
            temp = obj.VideoInput.VideoResolution;
            resolution_X = temp(1);
        end
        
        function resolution_Y = get.Y(obj)
            temp = obj.VideoInput.VideoResolution;
            resolution_Y = temp(2);
        end
        
        function resolution_color = get.C(obj)
            resolution_color = obj.VideoInput.NumberOfBands;
        end
        
        function frame_size = get.FrameSize(obj)
            frame_size = [obj.X obj.Y obj.C];
        end
        
        function option_str = get.OptionString(obj)
            res = obj.VideoInput.VideoResolution;
            option_str = ['format_' obj.VideoInput.VideoFormat ...
                          '_x_'     num2str(res(1)) ...
                          '_y_'     num2str(res(2)) ...
                          '_c_'     num2str(obj.VideoInput.NumberOfBands) ...
                          '_bri_'   num2str(obj.VideoSource.Brightness) ...
                          '_con_'   num2str(obj.VideoSource.Contrast) ...
                          '_exp_'   num2str(obj.VideoSource.Exposure) ...
                          '_fps_'   obj.VideoSource.FrameRate ...
                          '_gai_'   num2str(obj.VideoSource.Gain) ...
                          '_sat_'   num2str(obj.VideoSource.Saturation) ...
                          '_srp_'   num2str(obj.VideoSource.Sharpness) ...
                          '_whb_'   num2str(obj.VideoSource.WhiteBalance)];
        end
    end
    
    %% Public methods
    methods
        function data = GetSnapshot(obj)
            disp('Taking snapshot...');
            data = getsnapshot(obj.VideoInput);
            disp('Taking snapshot finished.');
        end
        
        function [] = Start(obj)
            disp('Starting capture...');
            start(obj.VideoInput);
            flushdata(obj.VideoInput);
            disp('Starting capture finished.');
        end
        
        function [data, time, info] = GetFramesFromBuffer(obj, count)
            
            assert(count <= obj.MaxFrameCount, 'CameraControl:GetFramesFromBuffer:ArgCount', 'The frame count is limited to %d frames.', int32(floor(obj.MaxFrameCount)));
            
            [data, time, info] = getdata(obj.VideoInput, count);
            
            persistent startTime;
            persistent lastFrameTime;
            
            if info(1).FrameNumber == 1
                startTime = time(1);
                lastFrameTime = time(1);
            end
            
            if length(time) > 1
                lastFrameTime = time(end-1);
            end
                
            fps_mean = (info(end).FrameNumber - 1) / (time(end) - startTime);
            fps_current = 1 / (time(end) - lastFrameTime);
            disp(['Fr ' num2str(info(end).FrameNumber) ...
                  ' @ ' num2str(time(end)-startTime) ...
                  ' | ' num2str(fps_mean) ' avg fps' ...
                  ' | ' num2str(fps_current) ' fps' ...
                  ' | ' num2str(obj.VideoInput.FramesAvailable) ' frames buffered']);
            
            lastFrameTime = time(end);
        end
        
        function [] = Stop(obj)
            disp('Terminating capture...');
            flushdata(obj.VideoInput);
            stop(obj.VideoInput);
            disp('Terminating capture finished.');
        end
    end
    
end

