classdef Hit < handle
    %HITS Hit data
    %   Obtain and analyse hit data from GeigerCam frames
    
    % Copyright 2014- Thomas Auzinger <thomas@auzinger.name>
    
    %% Constructor
    methods
        function obj = Hit(mask, image, energy)

            narginchk(0, 3);
            assert(nargin == 0 || nargin == 3, 'Hit:Constructor:NArgChk', 'Invalid input argument count.'); 
            
            if (nargin == 0)
                obj.Mask = logical.empty;
                obj.Image = double.empty;
                obj.Energy = NaN;
            elseif (nargin == 3)
                obj.Mask = mask;
                obj.Image = image;
                obj.Energy = energy;
            end
        end
    end
    
    
    %% Readonly public properties
    properties
        Mask
        Image
        Energy
    end
    
    %% Public static methods
    methods (Static = true)
        function [hits, pixelIdxList] = ExtractHits(mask, frame)
            MASK_DILATE = 3;
            BB_BORDER_DILATE = 3;
            GAMMA = 2.2;
          
            square = strel('square',MASK_DILATE);
            mask = imdilate(mask, square);
            
            components = bwconncomp(mask, 8);
            
            if components.NumObjects == 0
                hits = Hit.empty;
                pixelIdxList = double.empty;
                return
            end
            
            hits(1,components.NumObjects) = Hit;
            pixelIdxList = [];
            info = regionprops(components, 'PixelIdxList', 'BoundingBox');
            
            count = 0;
            for i = 1:components.NumObjects
                bb = info(i).BoundingBox;
                bb = floor(bb);
                
                bb(1:2) = max([1 1], bb(1:2)-BB_BORDER_DILATE);
                bb(3:4) = min([size(frame,2) size(frame,1)] - bb(1:2), bb(3:4)+2*BB_BORDER_DILATE);
                
                hit_mask  = mask (bb(2):bb(2)+bb(4), bb(1):bb(1)+bb(3));
                hit_image = frame(bb(2):bb(2)+bb(4), bb(1):bb(1)+bb(3), :);
                hit_energy = sum( power( double(hit_image(:))/255, GAMMA ) );
                
                if hit_energy >= Config.MinEnergy
                    count = count + 1;
                    hits(count) = Hit(hit_mask, hit_image, hit_energy);
                end
                
                pixelIdxList = cat(1, pixelIdxList, info(i).PixelIdxList);
            end
            
            hits = hits(1:count);
        end
        
        function [] = ShowHitImages(hits)
            showhits({hits.Image});
        end
    end
end

